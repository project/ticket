CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Recommended Modules
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Ticket module provides ticketing and registration for events (and more
abstractly, entities). Events may have multiple ticket types, each with a unique
set of requirements and registration fields.

 * For a full description of the module visit:
   https://www.drupal.org/project/ticket

* For a short video demo/overview of the module visit:
  https://www.youtube.com/watch?v=FTqCcLtAH-w

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/ticket


REQUIREMENTS
------------

This module requires the following outside of Drupal core.

 * Date - https://www.drupal.org/project/date
 * Entity - https://www.drupal.org/project/entity
 * Multiple Entity Form - https://www.drupal.org/project/multiple_entity_form
 * Views - https://www.drupal.org/project/views


RECOMMENDED MODULES
-------------------

 * Ctools (required for Ticket Registration States) -
   https://www.drupal.org/project/ctools
 * Entity Reference (required for Ticket Registration States) -
   https://www.drupal.org/project/entityreference
 * Rules (required for Ticket Rules) - https://www.drupal.org/project/rules


INSTALLATION
------------

 * Install the Ticket module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/895232 for further information.


CONFIGURATION
--------------

    1. Navigate to Administration > Modules and enable the module.
    2. Navigate to Administration > Structure > and add a Ticket type field to
       the content type where you will be providing tickets.
    3. Create new content, filling in the information for the ticket type.
       Your new content will have a Ticket field set. Select the tab to view
       information about the Ticket types.
    4. From this tab, you can also manage the fields for your Ticket types.
       These fields will be presented to users when they register for a ticket
       of the given Ticket type.


MAINTAINERS
-----------

 * Jakob Perry (japerry) - https://www.drupal.org/u/japerry
 * Emilie Nouveau (DyanneNova) - https://www.drupal.org/u/dyannenova
 * Jason Yee (jyee) - https://www.drupal.org/u/jyee

Supporting organizations:

 * Drupal Association - https://www.drupal.org/drupal-association
 * MongoDB - https://www.drupal.org/mongodb
